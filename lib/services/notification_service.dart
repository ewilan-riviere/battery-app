import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

class NotificationService {
  NotificationService({
    required this.notificationInstance,
    required this.id,
  });

  FlutterLocalNotificationsPlugin notificationInstance;
  int id = 0;

  static void snackbar(String message) {
    Get.snackbar(
      'Snackbar',
      message,
      snackPosition: SnackPosition.TOP,
      titleText: const Text(
        'Success!',
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      messageText: Text(
        message,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.green.shade600,
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.all(20),
      duration: const Duration(milliseconds: 800),
    );
  }

  static Future<void> notification(String message) async {
    var service = NotificationService(
      notificationInstance: FlutterLocalNotificationsPlugin(),
      id: 0,
    );

    await service.notificationInstance
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: false,
          sound: true,
        );

    const DarwinInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: false,
      requestSoundPermission: true,
    );

    const notifMacosDetails = DarwinNotificationDetails(
      sound: 'default',
      presentAlert: true,
    );

    const notificationDetails = NotificationDetails(
      macOS: notifMacosDetails,
    );

    service.id++;

    await service.notificationInstance.show(
      service.id,
      'Battery',
      message,
      notificationDetails,
    );
  }
}
