import 'dart:async';

import 'package:battery/package/package_device.dart';
import 'package:battery_plus/battery_plus.dart';
import 'package:get/get.dart';
import 'package:system_tray/system_tray.dart';

class BatteryController extends GetxController {
  final systemTray = SystemTray().obs;
  final _menuMain = Menu().obs;
  final _appWindow = AppWindow().obs;
  var device = PackageDevice();

  var battery = Battery().obs;
  var percentage = 0.obs;
  var state = BatteryState.full.obs;

  var notifLow = false.obs;
  var notifCritical = false.obs;

  var notifFull = false.obs;

  var percentageStr = "0".obs;
  var stateStr = "full".obs;

  var icon = "assets/icons/battery-50.png".obs;

  late Timer timer;

  BatteryState batteryState = BatteryState.full;
  late StreamSubscription streamSubscription;

  init() {
    getBatteryState();
    getBatteryPerentage();

    Timer.periodic(const Duration(seconds: 60), (timer) {
      getBatteryState();
      getBatteryPerentage();
    });
  }

  void getBatteryPerentage() async {
    final level = await battery.value.batteryLevel;
    percentage.value = level;
    percentageStr.value = "$level";
    setIcon();
    // print("Battery: ${percentage.value}");
  }

  void getBatteryState() async {
    streamSubscription = battery.value.onBatteryStateChanged.listen((newState) {
      batteryState = newState;
      state.value = newState;

      stateStr.value = newState.name;
      // print("State: ${state.value}");
    });
  }

  String iconBasePath() {
    var levelFive = (percentage.value.ceil() / 5).ceil() * 5;
    var levelStr = levelFive.toString();
    return "assets/icons/battery-$levelStr.png";
  }

  void setIcon() async {
    // icon.value = iconBasePath(level);

    // systemTray.value.setTitle(" ${percentageStr.value}%");
    // systemTray.value.setImage(icon.value);
  }

  void setMenu() async {
    device = await PackageDevice.make();

    await _menuMain.value.buildFrom(
      [
        MenuItemLabel(
          label: "${device.computerName}",
          enabled: false,
        ),
        MenuItemLabel(
          label: "Battery: ${percentageStr.value}%",
          enabled: false,
        ),
        MenuSeparator(),
        MenuItemLabel(
            label: 'Exit', onClicked: (menuItem) => _appWindow.value.close()),
      ],
    );

    systemTray.value.setContextMenu(_menuMain.value);
  }
}
