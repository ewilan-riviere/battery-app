import 'package:battery/package/package_device.dart';
import 'package:battery/controllers/battery_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:battery/views/widgets/app_layout.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final b = Get.put(BatteryController());
  var device = PackageDevice();

  @override
  void initState() {
    super.initState();
    b.init();
    PackageDevice.make().then((value) => device = value);
  }

  @override
  Widget build(BuildContext context) {
    b.percentageStr.listen((level) {
      setState(() {});
    });
    b.stateStr.listen((level) {
      setState(() {});
    });

    return AppLayout(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("${b.percentageStr.value}%"),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(
                children: [
                  Text(
                    "${device.computerName}",
                    style: const TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 2),
                    child: Text(
                      b.stateStr.value,
                      style: const TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
