import 'package:flutter/material.dart';
import 'package:battery/utils/utils_system_tray.dart';
import 'package:battery/views/widgets/top_bar.dart';

class AppLayout extends StatelessWidget {
  const AppLayout(
      {super.key, this.child = const SizedBox(), this.background = false});

  final bool background;

  final Widget child;

  @override
  Widget build(BuildContext context) {
    var st = UtilsSystemTray();
    st.initSystemTray();

    return Container(
      constraints: const BoxConstraints.expand(),
      child: Scaffold(
        backgroundColor: Colors.black.withOpacity(0.6),
        body: Container(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20,
            top: 35,
          ),
          child: child,
        ),
        floatingActionButton: FloatingActionButton.small(
          backgroundColor: Colors.grey.shade500,
          onPressed: () {
            // st.showNotification();
          },
          child: const Icon(
            Icons.toggle_off_outlined,
          ),
        ),
      ),
    );
  }
}
