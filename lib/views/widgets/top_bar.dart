import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopBar extends StatefulWidget with PreferredSizeWidget {
  const TopBar({Key? key}) : super(key: key);
  static const double _topBarHeight = 50;
  final double topBarHeight = _topBarHeight;

  @override
  State<TopBar> createState() => _TopBarState();

  @override
  Size get preferredSize => const Size.fromHeight(_topBarHeight);
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    // final b = Get.put(BatteryController());

    return AppBar(
      toolbarHeight: widget.topBarHeight,
      automaticallyImplyLeading: false,
      elevation: 0,
      backgroundColor: Colors.grey.shade900,
      actions: [
        iconAction(
          context: context,
          icon: Icons.toggle_off_outlined, // Icons.toggle_on_outlined
          onPressed: () {
            //
          },
        ),
      ],
    );
  }

  Widget iconAction({
    required BuildContext context,
    IconData icon = CupertinoIcons.add,
    Function? onPressed,
  }) {
    return IconButton(
      icon: Icon(
        icon,
        size: 20,
        color: Colors.grey,
      ),
      splashRadius: 20,
      onPressed: () {
        if (onPressed != null) {
          onPressed();
        }
      },
    );
  }
}
