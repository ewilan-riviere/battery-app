import 'package:battery/package/package_device.dart';
import 'package:battery/controllers/battery_controller.dart';
import 'package:battery/services/notification_service.dart';
import 'package:battery_plus/battery_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:battery/views/widgets/top_bar.dart';
import 'package:window_manager/window_manager.dart';
import 'package:tray_manager/tray_manager.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> notifications() async {
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          MacOSFlutterLocalNotificationsPlugin>()
      ?.requestPermissions(
        alert: true,
        badge: false,
        sound: true,
      );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await windowManager.ensureInitialized();

  WindowOptions windowOptions = const WindowOptions(
    size: Size(300, 300),
    maximumSize: Size(300, 300),
    minimumSize: Size(300, 300),
    center: true,
    backgroundColor: Colors.transparent,
    skipTaskbar: false,
    titleBarStyle: TitleBarStyle.hidden,
  );
  windowManager.waitUntilReadyToShow(windowOptions, () async {
    await windowManager.hide();
  });

  await notifications();

  runApp(
    const BatteryApp(),
  );
}

class BatteryApp extends StatelessWidget {
  const BatteryApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Battery',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TrayListener, WindowListener {
  final b = Get.put(BatteryController());
  var device = PackageDevice();

  List<MenuItem> menuItems = [
    MenuItem(
      key: 'exit_app',
      label: 'Exit',
    ),
  ];

  @override
  void initState() {
    super.initState();
    windowManager.addListener(this);
    trayManager.addListener(this);
    PackageDevice.make().then((value) => device = value);
    b.init();
    setMenu(menuItems);
  }

  @override
  void dispose() {
    windowManager.removeListener(this);
    trayManager.removeListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    b.percentageStr.listen((percentageStr) async {
      if (b.state.value == BatteryState.discharging) {
        await setIcon();
      }
      await trayManager.setIconPosition(TrayIconPositon.right);
      await trayManager.setTitle("$percentageStr % ");
      setState(() {});
    });
    b.state.listen((state) async {
      if (state == BatteryState.charging) {
        if (b.percentage.value >= 80) {
          await trayManager.setIcon('assets/icons/battery-charge-full.png');
        } else if (b.percentage.value >= 40) {
          await trayManager.setIcon('assets/icons/battery-charge-mid.png');
        } else {
          await trayManager.setIcon('assets/icons/battery-charge-none.png');
        }
      } else {
        await setIcon();
      }
      await trayManager.setIconPosition(TrayIconPositon.right);
      await trayManager.setTitle("${b.percentageStr} % ");
      setState(() {});
    });
    b.percentage.listen((percentage) async {
      if (b.state.value == BatteryState.discharging) {
        b.notifFull.value = false;
        if (percentage <= 10 && !b.notifCritical.value) {
          NotificationService.notification(
            "Battery is critical, please charge it",
          );
          b.notifCritical.value = true;
        }
        if (percentage <= 20 && !b.notifLow.value) {
          NotificationService.notification(
            "Battery is low, please charge it",
          );
          b.notifLow.value = true;
        }
      }
      if (b.state.value == BatteryState.charging) {
        b.notifCritical.value = false;
        b.notifLow.value = false;
        if (percentage >= 80 && !b.notifFull.value) {
          NotificationService.notification(
            "Battery is full, please unplug it",
          );
          b.notifFull.value = true;
        }
      }
      setState(() {});
    });

    return Scaffold(
      appBar: const TopBar(),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 0),
                child: Text(
                  "${b.percentageStr.value}%",
                  style: const TextStyle(fontSize: 12),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 6),
                child: Text(
                  "${device.computerName}",
                  style: const TextStyle(
                    fontSize: 15,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: Text(
                  b.stateStr.value,
                  style: const TextStyle(fontSize: 12),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onTrayIconMouseDown() {
    trayManager.popUpContextMenu();
  }

  @override
  void onTrayIconRightMouseDown() {
    // do something
  }

  @override
  void onTrayIconRightMouseUp() {
    // do something
  }

  @override
  void onTrayMenuItemClick(MenuItem menuItem) async {
    if (menuItem.key == 'exit_app') {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }
  }

  Future<void> setIcon() async {
    await trayManager.setIcon(b.iconBasePath());
  }

  Future<void> setMenu(List<MenuItem> items) async {
    await setIcon();
    var menu = Menu(items: items);
    await trayManager.setIconPosition(TrayIconPositon.right);
    await trayManager.setTitle("${b.percentageStr} % ");
    await trayManager.setToolTip('Battery');
    await trayManager.setContextMenu(menu);
  }
}
