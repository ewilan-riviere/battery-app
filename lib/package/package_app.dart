import 'package:package_info_plus/package_info_plus.dart';

class PackageApp {
  PackageApp();

  String appName = 'Unknown';
  String packageName = 'Unknown';
  String version = 'Unknown';
  String buildNumber = 'Unknown';
  String buildSignature = 'Unknown';
  String? installerStore = 'Unknown';

  static Future<PackageApp> make() async {
    final info = await PackageInfo.fromPlatform();
    var self = PackageApp();

    self.appName = info.appName;
    self.packageName = info.packageName;
    self.version = info.version;
    self.buildNumber = info.buildNumber;
    self.buildSignature = info.buildSignature;
    self.installerStore = info.installerStore;

    return self;
  }
}
