import 'dart:async';

import 'package:battery_plus/battery_plus.dart';

class PackageBattery {
  PackageBattery({
    required this.battery,
    this.state,
    this.stateSubscription,
  });

  Battery battery;
  BatteryState? state;
  StreamSubscription<BatteryState>? stateSubscription;

  static Future<PackageBattery> make() async {
    var self = PackageBattery(
      battery: Battery(),
    );

    var newState = await self.battery.batteryState;
    self.updateState(newState);

    self.stateSubscription =
        self.battery.onBatteryStateChanged.listen(self.updateState);

    return self;
  }

  void updateState(BatteryState newState) {
    if (state == newState) return;
    state = newState;
  }
}
