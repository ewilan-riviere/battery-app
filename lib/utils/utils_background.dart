import 'package:flutter/material.dart';

class UtilsBackground extends StatelessWidget {
  const UtilsBackground({
    Key? key,
    // this.children = const <Widget>[],
    // this.floatingActionButton,
    // this.floatingActionButtonLocation,
    required this.widget,
  }) : super(key: key);

  // final List<Widget> children;
  // final Widget? floatingActionButton;
  // final FloatingActionButtonLocation? floatingActionButtonLocation;
  final Widget widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints.expand(),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/icons/index.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: widget,
      // backgroundColor: Colors.grey.shade900.withOpacity(0.6),
      //   body: Center(
      //     child: scaffold.body,
      //   ),
      //   floatingActionButton: scaffold.floatingActionButton,
      //   floatingActionButtonLocation: scaffold.floatingActionButtonLocation,
    );
  }
}
