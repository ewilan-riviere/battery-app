import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
var id = 0;

class UtilsNotification {
  static void snackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        elevation: 2,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        margin: const EdgeInsets.only(bottom: 20.0, left: 50.0, right: 50.0),
        dismissDirection: DismissDirection.none,
        behavior: SnackBarBehavior.floating,
        duration: const Duration(seconds: 1),
      ),
    );
  }

  static Future<void> showNotification({
    String? title,
    String message = '',
  }) async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: false,
          sound: true,
        );

    const initializationSettingsDarwin = DarwinInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: false,
      requestSoundPermission: true,
    );

    var macOSNotificationDetails = const DarwinNotificationDetails(
      presentAlert: true,
      sound: 'default',
      subtitle: 'Battery',
    );

    NotificationDetails notificationDetails = NotificationDetails(
      // android: androidNotificationDetails,
      // iOS: iosNotificationDetails,
      macOS: macOSNotificationDetails,
      // linux: linuxNotificationDetails,
    );

    id++;

    await flutterLocalNotificationsPlugin.show(
      id,
      title,
      message,
      notificationDetails,
    );
  }
}
