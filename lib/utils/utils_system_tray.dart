import 'dart:async';
import 'dart:io';

import 'package:battery/controllers/battery_controller.dart';
import 'package:get/get.dart';
import 'package:system_tray/system_tray.dart';

class UtilsSystemTray {
  final BatteryController b = Get.put(BatteryController());

  final AppWindow _appWindow = AppWindow();

  String getTrayImagePath(String imageName) => Platform.isWindows
      ? 'assets/icons/$imageName.ico'
      : 'assets/icons/$imageName.png';

  String getImagePath(String imageName) => Platform.isWindows
      ? 'assets/icons/$imageName.bmp'
      : 'assets/icons/$imageName.png';

  Future<void> initSystemTray() async {
    await b.systemTray.value.initSystemTray(
      iconPath: b.icon.value,
      toolTip: b.percentageStr.value,
    );

    b.systemTray.value.setImage(b.icon.value);
    b.systemTray.value.setToolTip("${b.percentageStr.value}%");

    b.systemTray.value.registerSystemTrayEventHandler((eventName) {
      if (eventName == kSystemTrayEventClick) {
        Platform.isWindows
            ? _appWindow.show()
            : b.systemTray.value.popUpContextMenu();
      } else if (eventName == kSystemTrayEventRightClick) {
        Platform.isWindows
            ? b.systemTray.value.popUpContextMenu()
            : _appWindow.show();
      }
    });

    b.setMenu();
  }
}
