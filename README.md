# Battery

[![flutter](https://img.shields.io/static/v1?label=Flutter&message=v3.7&color=02569B&style=flat-square&logo=flutter&logoColor=ffffff)](https://flutter.dev)

## Clean

```bash
flutter clean ; flutter pub get ; cd ios ; pod install ; cd ..
```

## App

```bash
cp -r build/macos/Build/Products/Release/Battery.app /Applications
```

### dmg

```bash
npm install -g appdmg
```
